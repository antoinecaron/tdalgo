/**
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 *
 * @brief :
 *
 **/

#include <string.h>

#include <TDAlgo/TDAlgo.h>
#include <TDAlgo/TDErrors.h>


static void _TD_memzero(void *to, uint32_t size);

void _TD_memzero(void *to, uint32_t size) {
	memset(to, 0, size);
}


void TD_memzero(void *to, uint32_t size) {
	TD_THROW_IF(!to, exit);

	_TD_memzero(to, size);

exit:
	return;
}

void TD_copyMemory(void *dst, const void *src, uint32_t length, bool reverse) {
	TD_THROW_IF(!dst, exit);

	if (src) {
		for (uint32_t idx = length; idx--;) {
			uint32_t index = reverse ? (length - 1) - idx : idx;
			((uint8_t *) dst)[idx] = ((uint8_t *) src)[index];
		}
	} else {
		TD_memzero(dst, length);
	}

exit:
	return;
}

bool TD_isMemZero(const void *ptr, uint32_t len) {
	bool isMemZero = true;
	const uint8_t *p = (const uint8_t *) ptr;

	for (; p && len; --len, ++p) {
		if (*p) {
			isMemZero = false;
			TD_THROW(exit)
		}
	}

exit:
	return isMemZero;
}

bool TD_isMemEqual(const void *ptr1, const void *ptr2, uint32_t len) {
	return (ptr1 && ptr2) ? (memcmp(ptr1, ptr2, len) == 0)
								 : TD_isMemZero((void *const)(((uintptr_t) ptr1) | ((uintptr_t) ptr2)), len);
}


void TD_xorMask(uint8_t buffer[], uint8_t lenBuffer, const uint8_t mask[], uint16_t maskLen) {
	for (uint16_t i = 0; i < lenBuffer; ++i) {
		buffer[i] ^= mask[i % maskLen];
	}
}

bool TD_isLuhnValid(const char *str) {
	const int m[] = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };   // mapping for rule 3
	int i, odd = 1, sum = 0;

	for (i = (int) strlen(str); i--; odd = !odd) {
		int digit = str[i] - '0';
		sum += odd ? digit : m[digit];
	}
	return (sum % 10 == 0);
}
