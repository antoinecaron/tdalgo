/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 ****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <TDAlgo/TDAlgo.h>
#include <TDAlgo/TDConvert.h>


uint8_t TD_char2dec(char c) {
	return (uint8_t)((isdigit(c)) ? c - '0' : 0);
}

char TD_dec2char(uint8_t value) {
	return (char) ('0' + (value % 10));
}

uint8_t TD_char2hex(char c) {
	// clang-format off
	return (uint8_t) (
	isdigit(c)
	? (c - '0')
	: (c >= 'a' && c <= 'f')
	  ? (c - 'a' + 0xa)
	  : (c >= 'A' && c <= 'F')
		 ? (c - 'A' + 0xa)
		 : 0xFF
	);
	// clang-format on
}

char TD_hex2char(uint8_t value) {
	value &= 0xF;
	return (char) (value < 0xA ? '0' + value : 'A' + value - 0xa);
}

void TD_str2hex(const char *str, uint8_t *buffer, uint16_t *pLength) {
	uint16_t idxStr = 0;
	uint16_t idxBuffer = 0;
	uint16_t strLen = (uint16_t) strlen(str);
	TD_memzero(buffer, *pLength);
	for (; idxStr < strLen && idxBuffer / 2 < *pLength; ++idxStr) {
		char c = str[idxStr];
		if (isxdigit(c)) {
			buffer[idxBuffer / 2] |= (TD_char2hex(c) & 0x0Fu) << (4u * ((idxBuffer + 1u) % 2u));
			idxBuffer += 1;
		}
	}
	*pLength = (uint16_t)((idxBuffer + 1) / 2);
}

void TD_hex2str(const uint8_t *buffer, uint16_t length, char *str) {
	for (int i = 0; i < length; ++i) {
		sprintf(str + 2 * i, "%02x", buffer[i]);
	}
}
