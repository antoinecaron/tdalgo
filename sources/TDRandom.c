/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 ****************************************************************************/

#include <stdlib.h>
#include <time.h>

#include <TDAlgo/TDErrors.h>
#include <TDAlgo/TDAlgo.h>
#include <TDAlgo/TDRandom.h>


static void _TDUtils_srand();

void _TDUtils_srand() {
	static bool started = false;
	TD_THROW_IF(started, exit)

	srand((unsigned int) time(NULL));
	started = true;

exit:
	return;
}

uint16_t TDUtils_random(uint16_t max, uint16_t min) {
	_TDUtils_srand();
	return (uint16_t)((rand() % (max - min)) + min);
}

void TDUtils_randomBuffer(uint8_t buffer[], uint16_t len) {
	_TDUtils_srand();
	for (uint16_t i = 0; i < len; ++i) {
		buffer[i] = (uint8_t) rand();
	}
}

void TDUtils_randomString(char buffer[], uint16_t len) {
	static const char letters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	TDUtils_randomSymbols(buffer, len, letters, sizeof(letters) - 1);
}

void TDUtils_randomSymbols(char buffer[], uint16_t len, const char symbols[], uint16_t symbolsLen) {
	_TDUtils_srand();
	for (uint16_t i = 0; i < len; ++i) {
		buffer[i] = symbols[TDUtils_random(symbolsLen, 0)];
	}
}
