/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 ****************************************************************************/

#ifndef TDALGO_TDRANDOM_H
#define TDALGO_TDRANDOM_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


uint16_t TDUtils_random(uint16_t max, uint16_t min);
void TDUtils_randomBuffer(uint8_t buffer[], uint16_t len);
void TDUtils_randomString(char buffer[], uint16_t len);
void TDUtils_randomSymbols(char buffer[], uint16_t len, const char symbols[], uint16_t symbolsLen);


#ifdef __cplusplus
}
#endif

#endif   // TDALGO_TDRANDOM_H
