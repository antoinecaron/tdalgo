/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 10 08 2018
 ****************************************************************************/

#ifndef TDALGO_TDERRORS_H
#define TDALGO_TDERRORS_H

#include <stdint.h>

typedef uint32_t ErrorCode;

#define TD_SUCCESS 0
#define TD_ERROR_DEFAULT 1
#define TD_ERROR_UNEXPETED 2
#define TD_ERROR_PROGRAMING_FAULT 3
#define TD_ERROR_NOT_YET_IMPLEMENTED 4

#define TD_ERROR_INITIALIZED 100
#define TD_ERROR_NOT_INITIALIZED 101

#define TD_ERROR_INVALID 110
#define TD_ERROR_INVALID_PARAMETER 110
#define TD_ERROR_INVALID_TYPE 111

#define TD_ERROR_EXIST 112
#define TD_ERROR_DOESNT_EXIST 113

#define TD_ERROR_EQUAL 112
#define TD_ERROR_NOT_EQUAL 113


// TODO
typedef struct _TDError {
	ErrorCode errorCode;
	char* message;
	struct _TDError* pCause;
} TDError;

/**
 * @def TD_DO_IF(__IF__, __DO__)
 * @param __IF__ expression to check
 * @param __DO__ action if expression is evaluated to TRUE
 * @details Inline "if then do" action
 */
#define TD_DO_IF(__IF__, __DO__) \
	{                             \
		if ((__IF__)) {            \
			__DO__                  \
		}                          \
	}

#define TD_THROW(__JUMP__) \
	{ goto __JUMP__; }

#define TD_THROW_ERROR(__ERROR_CODE__, __JUMP__) \
	{                                             \
		errorCode = ((int) (__ERROR_CODE__));      \
		TD_THROW(__JUMP__)                         \
	}

#define TD_THROW_IF(__IF__, __JUMP__) TD_DO_IF(__IF__, TD_THROW(__JUMP__))
#define TD_THROW_ERROR_IF(__IF__, __ERROR_CODE__, __JUMP__) TD_DO_IF(__IF__, TD_THROW_ERROR(__ERROR_CODE__, __JUMP__))

#define TD_THROW_IF_ERROR(__JUMP__) TD_THROW_IF(errorCode, __JUMP__)

#define TD_IGNORE_ERROR \
	{                    \
		errorCode = 0;    \
		(void) errorCode; \
	}

#endif   // TDALGO_TDERRORS_H
