/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 ****************************************************************************/

#ifndef TDALGO_TDALGO_H
#define TDALGO_TDALGO_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef RETURN_NOT_USED
#	define RETURN_NOT_USED (void)
#endif

#ifndef NOT_USED
#	define NOT_USED(X) (RETURN_NOT_USED(X))
#endif

#define IS_LITTLE_ENDIAN \
	((union {             \
		 uint16_t x;       \
		 uint8_t y;        \
	 }){ 1 }              \
		 .y)

#define LENGTH_OF(__TABLE__) (sizeof(__TABLE__) / sizeof((__TABLE__)[0]))
#define INDEX_OF(__PTR__, __TABLE__, __TYPE__) ((uint8_t)((((intptr_t)(__PTR__)) - ((intptr_t)(__TABLE__))) / sizeof(__TYPE__)))

#define TD_MIN(__1, __2) (((__1) < (__2)) ? (__1) : (__2))
#define TD_MAX(__1, __2) (((__1) > (__2)) ? (__1) : (__2))

#define TD_LOGICAL_XOR(__1, __2) ((!(__1)) ^ (!(__2)))

#define TD_REBOUND(x, limit) ((((x) % (limit)) + (limit)) % (limit))


void TD_memzero(void *to, uint32_t size);
void TD_copyMemory(void *dst, const void *src, uint32_t length, bool reverse);

bool TD_isMemEqual(const void *ptr1, const void *ptr2, uint32_t len);

bool TD_isMemZero(const void *ptr, uint32_t len);


/**
 * Apply xor mask on specified buffer. mask is repeated if necessary.
 * <br/>x 0 1
 * <br/>0 0 1
 * <br/>1 1 0
 * @param buffer [in, out]
 * @param lenBuffer [in]
 * @param mask [in]
 * @param maskLen [in]
 */
void TD_xorMask(uint8_t buffer[], uint8_t lenBuffer, const uint8_t mask[], uint16_t maskLen);

bool TD_isLuhnValid(const char *str);

#ifdef __cplusplus
}
#endif

#endif   // TDALGO_TDALGO_H
