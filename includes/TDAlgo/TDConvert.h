/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 ****************************************************************************/

#ifndef TDALGO_TDCONVERT_H
#define TDALGO_TDCONVERT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <ctype.h>

#ifdef __cplusplus
extern "C" {
#endif


uint8_t TD_char2dec(char c);
char TD_dec2char(uint8_t value);

uint8_t TD_char2hex(char c);
char TD_hex2char(uint8_t value);
void TD_str2hex(const char *str, uint8_t *buffer, uint16_t *pLength);
void TD_hex2str(const uint8_t *buffer, uint16_t length, char *str);


/*
 * Helper for quick converter for uint8_t[] to uint32_t and uint16_t or reverse
 * Depends on processor Big ou Little endianness
 */
#define SHORT2BYTES_BIG(s, a)                         \
	{                                                  \
		*(((uint8_t *) (a)) + 0) = (((s) >> 8) & 0xFF); \
		*(((uint8_t *) (a)) + 1) = (((s) >> 0) & 0xFF); \
	}
#define BYTES2SHORT_V_BIG(a0, a1) ((uint16_t)((((uint8_t)(a0)) << 8) | (((uint8_t)(a1)) << 0)))
#define BYTES2SHORT_BIG(a) BYTES2SHORT_V_BIG( \
	((uint8_t*)(a))[0], \
	((uint8_t*)(a))[1]  \
))

#define SHORT2BYTES_LITTLE(s, a)                      \
	{                                                  \
		*(((uint8_t *) (a)) + 0) = (((s) >> 0) & 0xFF); \
		*(((uint8_t *) (a)) + 1) = (((s) >> 8) & 0xFF); \
	}
#define BYTES2SHORT_V_LITTLE(a0, a1) ((uint16_t)((((uint8_t)(a0)) << 0) | (((uint8_t)(a1)) << 8)))
#define BYTES2SHORT_LITTLE(a) BYTES2SHORT_V_LITTLE( \
	((uint8_t*)(a))[0], \
	((uint8_t*)(a))[1]  \
))


#define LONG2BYTES_BIG(l, a)                  \
	{                                          \
		SHORT2BYTES_BIG((l) >> 16, a)           \
		SHORT2BYTES_BIG(l, (uint8_t *) (a) + 2) \
	}
#define BYTES2LONG_V_BIG(a0, a1, a2, a3) ((uint32_t)(BYTES2SHORT_V_BIG(a0, a1) << 16 | BYTES2SHORT_V_BIG(a2, a3)))
#define BYTES2LONG_BIG(a) \
	BYTES2LONG_V_BIG(((uint8_t *) (a))[0], ((uint8_t *) (a))[1], ((uint8_t *) (a))[2], ((uint8_t *) (a))[3])

#define LONG2BYTES_LITTLE(l, a)                          \
	{                                                     \
		SHORT2BYTES_LITTLE(l, a)                           \
		SHORT2BYTES_LITTLE((l) >> 16, (uint8_t *) (a) + 2) \
	}
#define BYTES2LONG_V_LITTLE(a0, a1, a2, a3) \
	((uint32_t)(BYTES2SHORT_V_LITTLE(a0, a1) | BYTES2SHORT_V_LITTLE(a2, a3) << 16))
#define BYTES2LONG_LITTLE(a) \
	BYTES2LONG_V_LITTLE(((uint8_t *) (a))[0], ((uint8_t *) (a))[1], ((uint8_t *) (a))[2], ((uint8_t *) (a))[3])

#if (uint64_t != uint32_t)

#	define LONGLONG2BYTES_BIG(l, a)             \
		{                                         \
			LONG2BYTES_BIG((l) >> 32, a)           \
			LONG2BYTES_BIG(l, (uint8_t *) (a) + 4) \
		}
#	define BYTES2LONGLONG_V_BIG(a0, a1, a2, a3, a4, a5, a6, a7) \
		((uint64_t)(BYTES2LONG_V_BIG(a0, a1, a2, a3) << 32 | BYTES2LONG_V_BIG(a4, a5, a6, a7)))
#	define BYTES2LONGLONG_BIG(a) \
		BYTES2LONGLONG_V_BIG(      \
			((uint8_t *) (a))[0],   \
			((uint8_t *) (a))[1],   \
			((uint8_t *) (a))[2],   \
			((uint8_t *) (a))[3],   \
			((uint8_t *) (a))[4],   \
			((uint8_t *) (a))[5],   \
			((uint8_t *) (a))[6],   \
			((uint8_t *) (a))[7])

#	define LONGLONG2BYTES_LITTLE(ll, a)                    \
		{                                                    \
			LONG2BYTES_LITTLE(l, a)                           \
			LONG2BYTES_LITTLE((l) >> 32, (uint8_t *) (a) + 4) \
		}
#	define BYTES2LONGLONG_V_LITTLE(a0, a1, a2, a3, a4, a5, a6, a7) \
		((uint64_t)(BYTES2LONG_V_LITTLE(a0, a1, a2, a3) | BYTES2LONG_V_LITTLE(a4, a5, a6, a7) << 32))
#	define BYTES2LONGLONG_LITTLE(a) \
		BYTES2LONGLONG_V_LITTLE(      \
			((uint8_t *) (a))[0],      \
			((uint8_t *) (a))[1],      \
			((uint8_t *) (a))[2],      \
			((uint8_t *) (a))[3],      \
			((uint8_t *) (a))[4],      \
			((uint8_t *) (a))[5],      \
			((uint8_t *) (a))[6],      \
			((uint8_t *) (a))[7])

#else

#	define LONGLONG2BYTES_BIG(l, a) LONG2BYTES_BIG(l, a)
#	define BYTES2LONGLONG_BIG(a) BYTES2LONG_BIG(a)
#	define BYTES2LONGLONG_V_BIG(a0, a1, a2, a3, a4, a5, a6, a7) BYTES2LONG_V_BIG(a0, a1, a2, a3)
#	define LONGLONG2BYTES_LITTLE(ll, a) LONG2BYTES_LITTLE(ll, a)
#	define BYTES2LONGLONG_LITTLE(a) BYTES2LONG_LITTLE(a)
#	define BYTES2LONGLONG_V_LITTLE(a0, a1, a2, a3, a4, a5, a6, a7) BYTES2LONG_V_LITTLE(a0, a1, a2, a3)
#endif

#ifdef __cplusplus
}
#endif

#endif   // TDALGO_TDCONVERT_H
