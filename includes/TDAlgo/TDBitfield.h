/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 18 05 2018
 ****************************************************************************/

#ifndef TDALGO_TDBITFIELD_H
#define TDALGO_TDBITFIELD_H

/**
 * @def TD_BIT_SET(__IDX)
 * @param __IDX bit index (start from 0) to set.
 * @return int (2^__IDX), pow(2, __IDX), (1 << __IDX)
 * @details ex : TD_BIT_SET(7) -> 0b1000_0000
 */
#define TD_BITFIELD_SET_BIT(__IDX) (1 << (__IDX))
#define TD_BITFIELD_GET_BIT(__VAL, __IDX) (((__VAL) >> (__IDX)) & 1)

/**
 * @def TD_BITMASK(__COUNT)
 * @param __COUNT Number of less significant bits to set to 1.
 * @return int Bitfield where all bits from 0 to __COUNT - 1 are set to 1.
 * @details ex : TD_BITMASK(6) -> 0b0011_1111
 */
#define TD_BITFIELD_SET_FIELD(__BITS_COUNT) (TD_BITFIELD_SET_BIT(__BITS_COUNT) - 1)
#define TD_BITFIELD_SET_FIELD_AT(__IDX, __BITS_COUNT) (TD_BITFIELD_SET_FIELD(__BITS_COUNT) << (__IDX))

/**
 * @def TD_BITFIELD_GET_FIELD
 * @param __VAL uint, values to get.
 * @param __IDX uint, bit index (start from 0)
 * @param __BITS_COUNT uint, number of bits to get.
 * @return the bits 'idx' to 'idx + bits' of 'x' value positioned at 0 to 'bits' bits
 * @details ex : TD_BITS_GET(0b1101_0110, 3, 2) -> 0b0001_1010 & 0b0000_0011 -> 0b0000_0010
 */
#define TD_BITFIELD_GET_FIELD(__VAL, __IDX, __BITS_COUNT) (((__VAL) >> (__IDX)) & TD_BITFIELD_SET_FIELD(__BITS_COUNT))
#define TD_BITFIELD_GET_FIELD_AT(__VAL, __IDX, __BITS_COUNT, __OUT_IDX) (TD_BITFIELD_GET_FIELD(__VAL, __IDX, __BITS_COUNT) << (__OUT_IDX))

/**
 * @def TD_BITFIELD_ASSIGN
 * @param x uint, values to keep.
 * @param y uint, values to set.
 * @param bits uint, number of bits to set.
 * @param idx uint, bit index (start from 0)
 * @return the 'x' value where the bits 'idx' to 'idx + bits' are setted to 'y' bits 0 to 'bits' bits
 * @details ex : TD_BITS_SET(0b0000_1111, 0b0000_1101, 3, 4) -> 0b0000_1111, 0b0101_0000 -> 0b0101_1111
 */
#define TD_BITFIELD_ASSIGN(__TO, __FROM, __IDX, __BITS_COUNT) \
(((__TO) & (~TD_BITFIELD_SET_FIELD_AT(__IDX, __BITS_COUNT))) | TD_BITFIELD_GET_FIELD_AT(__FROM, 0, __BITS_COUNT, __IDX))

#endif   // TDALGO_TDBITFIELD_H
