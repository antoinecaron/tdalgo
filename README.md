# TDAlgo

C Libray including some generics tools

- Basic tools
	- Min/Max
	- xor tools
	- Sized array handling
- Memory managment
	- Memory assignment
	- Memory compare
- Errors workflow
- Bitfield handling
- Random generator
	- Integer
- Cryptographic algorithm
	- AES
	- DES


## Errors workflow

``` C
ErrorCode notImplemented(void* pContext) {
	ErrorCode errorCode = TD_SUCCESS;

	TD_THROW_ERROR(TD_ERROR_NOT_YET_IMPLEMENTED, exit);

exit:
	return errorCode;
}
```

``` C
ErrorCode checkParams(void* pContext) {
	ErrorCode errorCode = TD_SUCCESS;

	TD_THROW_ERROR_IF(!pContext, TD_ERROR_INVALID, exit);

exit:
	return errorCode;
}
```

``` C
ErrorCode forward() {
	ErrorCode errorCode = TD_SUCCESS;

	errorCode = checkParams(NULL)
	TD_THROW_IF_ERROR(exit);

exit:
	return errorCode;
}
```

``` C
void ignore(void* pContext) {
	TD_THROW_IF(!pContext, exit);

exit:
	return;
}

```
