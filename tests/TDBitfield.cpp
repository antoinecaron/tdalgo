
#include <cstring>
#include <gtest/gtest.h>

#include <TDAlgo/TDBitfield.h>

const uint8_t uint32BitsCount = 8 * sizeof(uint32_t);

TEST(bitfield_Tests, bit_set) {
	uint32_t pow2 = 1;
	for (uint8_t idx = 0; idx < uint32BitsCount; ++idx) {
		ASSERT_EQ(pow2, TD_BITFIELD_SET_BIT(idx));
		pow2 *= 2;
	}
}

TEST(bitfield_Tests, field_set) {
	uint32_t field = 0;
	for (uint8_t idx = 0; idx < uint32BitsCount; ++idx) {
		ASSERT_EQ(field, TD_BITFIELD_SET_FIELD(idx));
		field = field << 1 | 1;
	}
}

TEST(bitfield_Tests, field_set_at) {
	uint32_t field = 7;
	for (uint8_t idx = 0; idx < uint32BitsCount; ++idx) {
		ASSERT_EQ(field, TD_BITFIELD_SET_FIELD_AT(idx, 3));
		field = field << 1;
	}
}

TEST(bitfield_Tests, bit_get) {
	uint32_t pow2 = 1;
	for (uint8_t i = 0; i < 32; ++i) {
		for (uint8_t idx = 0; idx < uint32BitsCount; ++idx) {
			ASSERT_EQ(i == idx, TD_BITFIELD_GET_BIT(pow2, idx));
		}
		pow2 *= 2;
	}
}

TEST(bitfield_Tests, field_get) {
	uint8_t bitsCount = 3;
	uint32_t field = TD_BITFIELD_SET_FIELD(bitsCount);
	for (uint8_t idx = 0; idx <= uint32BitsCount - bitsCount; ++idx) {
		ASSERT_EQ(TD_BITFIELD_SET_FIELD(bitsCount), TD_BITFIELD_GET_FIELD(field, idx, bitsCount));
		field = field << 1;
	}
}

TEST(bitfield_Tests, assign_1) {
	uint32_t x = 0b00001111;
	uint32_t y = 0b00001101;
	uint32_t r = 0b10101111;
	ASSERT_EQ(r, TD_BITFIELD_ASSIGN(x, y, 5, 3));
}

TEST(bitfield_Tests, assign_2) {
	uint32_t x = 0b11111111;
	uint32_t y = 0b00000000;
	uint32_t r = 0b11110011;
	ASSERT_EQ(r, TD_BITFIELD_ASSIGN(x, y, 2, 2));
}

TEST(bitfield_Tests, assign_3) {
	uint32_t x = 0b10101010;
	uint32_t y = 0b01101100;
	uint32_t r = 0b11001010;
	ASSERT_EQ(r, TD_BITFIELD_ASSIGN(x, y, 4, 3));
}