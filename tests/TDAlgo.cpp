/**
 * Copyright (c) 2016 Trust Designer
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @project : 	TDUtilsTest
 * @file : 		TDAlgo.cpp
 *
 * @author: Antoine Caron
 * @brief :
 **/

#include <cstring>
#include <gtest/gtest.h>

#include <TDAlgo/TDAlgo.h>
#include <TDAlgo/TDErrors.h>


TEST(endianness_Tests, isLittleEndian) {
	uint32_t value = 0x11223344;

	if (IS_LITTLE_ENDIAN) {
		ASSERT_TRUE((uint8_t) value == ((uint8_t *) &value)[0]);
	} else {
		ASSERT_TRUE((uint8_t) value == ((uint8_t *) &value)[3]);
	}
}

TEST(TDAlgo_Tests, LENGTH_OF) {
	const uint16_t size = 16;
	uint8_t byteTable[size];
	uint16_t shortTable[size];
	uint32_t intTable[size];
	struct {
		uint8_t a;
		uint32_t b;
		struct {
			union {
				char *str;
				void *ptr;
			};
			float f;
		} c;
	} structTable[size];
	ASSERT_EQ(LENGTH_OF(byteTable), size);
	ASSERT_EQ(LENGTH_OF(shortTable), size);
	ASSERT_EQ(LENGTH_OF(intTable), size);
	ASSERT_EQ(LENGTH_OF(structTable), size);
}

TEST(TDAlgo_Tests, INDEX_OF) {
	const uint16_t size = 16;
	struct test_st {
		uint8_t a;
		uint32_t b;
		struct {
			union {
				char *str;
				void *ptr;
			};
			float f;
		} c;
	} structTable[size];

	for (uint16_t i = size; i--;) {
		struct test_st *pItem = &structTable[i];
		ASSERT_EQ(INDEX_OF(pItem, structTable, struct test_st), i);
	}
}

TEST(TDAlgo_Tests, TD_MIN_MAX) {
	int min = 8;
	int max = 16;
	ASSERT_EQ(TD_MIN(min, max), min);
	ASSERT_EQ(TD_MIN(max, min), min);
	ASSERT_EQ(TD_MAX(min, max), max);
	ASSERT_EQ(TD_MAX(max, min), max);
}

TEST(TDAlgo_Tests, TD_LOGICAL_XOR) {
	ASSERT_EQ(TD_LOGICAL_XOR(0, 12), true);
	ASSERT_EQ(TD_LOGICAL_XOR(2, 1), false);
	ASSERT_EQ(TD_LOGICAL_XOR(0, 0), false);
}

TEST(TDAlgo_Tests, TD_REBOUND) {
}

TEST(isMemZeroTests, null_pointer) {
	ASSERT_TRUE(TD_isMemZero(nullptr, 0));
}

TEST(isMemZeroTests, null_length) {
	uint8_t table[] = { 1, 2, 3, 4 };
	ASSERT_TRUE(TD_isMemZero(table, 0));
}

TEST(isMemZeroTests, true) {
	uint8_t table[] = { 0, 0, 0, 0 };
	ASSERT_TRUE(TD_isMemZero(table, sizeof(table)));
}

TEST(isMemZeroTests, false) {
	uint8_t table[] = { 1, 2, 3, 4 };
	ASSERT_FALSE(TD_isMemZero(table, sizeof(table)));
}

TEST(isMemZeroTests, part_true) {
	uint8_t table[] = { 1, 2, 3, 4, 0, 0, 0, 0, 1, 2, 3, 4 };
	ASSERT_TRUE(TD_isMemZero(&table[4], 4));
}

TEST(isMemZeroTests, part_false) {
	uint8_t table[] = { 1, 2, 3, 4, 0, 0, 0, 0, 1, 2, 3, 4 };
	ASSERT_FALSE(TD_isMemZero(&table[2], 4));
}


TEST(isMemEqualTests, both_null_pointer) {
	ASSERT_TRUE(TD_isMemEqual(nullptr, nullptr, 0));
}

TEST(isMemEqualTests, second_null_pointer) {
	uint8_t table[] = { 0, 0, 0, 0 };
	ASSERT_TRUE(TD_isMemEqual(table, nullptr, sizeof(table)));
}

TEST(isMemEqualTests, first_null_pointer) {
	uint8_t table[] = { 0, 0, 0, 0 };
	ASSERT_TRUE(TD_isMemEqual(nullptr, table, sizeof(table)));
}

TEST(isMemEqualTests, second_null_pointer_false) {
	uint8_t table[] = { 1, 4, 8, 2 };
	ASSERT_FALSE(TD_isMemEqual(table, nullptr, sizeof(table)));
}

TEST(isMemEqualTests, first_null_pointer_false) {
	uint8_t table[] = { 1, 4, 8, 2 };
	ASSERT_FALSE(TD_isMemEqual(nullptr, table, sizeof(table)));
}

TEST(isMemEqualTests, null_length) {
	uint8_t table1[] = { 1, 4, 8, 2 };
	uint8_t table2[] = { 3, 5, 8, 7 };
	ASSERT_TRUE(TD_isMemEqual(table1, table2, 0));
}

TEST(isMemEqualTests, false) {
	uint8_t table1[] = { 1, 4, 8, 2 };
	uint8_t table2[] = { 3, 5, 8, 7 };
	ASSERT_FALSE(TD_isMemEqual(table1, table2, sizeof(table1)));
}

TEST(isMemEqualTests, true) {
	uint8_t table[] = { 1, 4, 8, 2 };
	ASSERT_TRUE(TD_isMemEqual(table, table, sizeof(table)));
}


TEST(memzeroTests, null_pointer) {
	TD_memzero(nullptr, 0);
	ASSERT_TRUE(true);
}

TEST(memzeroTests, null_length) {
	uint8_t table[] = { 1, 2, 3, 4 };
	TD_memzero(table, 0);

	for (uint8_t i = 0; i < sizeof(table); ++i) {
		ASSERT_EQ(i + 1, table[i]);
	}
}
