/**
 * Copyright (c) 2016 Trust Designer
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @project : 	TDUtilsTest
 * @file : 		TDAlgo.cpp
 *
 * @author: Antoine Caron
 * @brief :
 **/

#include <cstring>
#include <gtest/gtest.h>

#include <TDAlgo/TDErrors.h>


TEST(DoIfTest, true) {
	bool done = false;
	TD_DO_IF(true, done = true;)
	ASSERT_TRUE(done);
}

TEST(DoIfTest, false) {
	bool done = true;
	TD_DO_IF(false, done = false;)
	ASSERT_TRUE(done);
}

TEST(ThrowTest, success) {
	TD_THROW(success);
	ASSERT_FALSE(true);

success:
	ASSERT_TRUE(true);
}

TEST(ThrowErrorTest, success) {
	int errorCode = TD_SUCCESS;

	TD_THROW_ERROR(TD_ERROR_DEFAULT, success)
	ASSERT_FALSE(true);

success:
	ASSERT_EQ(errorCode, TD_ERROR_DEFAULT);
}

TEST(ThrowErrorIfTest, true) {
	int errorCode = TD_SUCCESS;

	TD_THROW_ERROR_IF(true, TD_ERROR_DEFAULT, success)
	ASSERT_FALSE(true);

success:
	ASSERT_EQ(errorCode, TD_ERROR_DEFAULT);
}

TEST(ThrowErrorIfTest, false) {
	int errorCode = TD_SUCCESS;

	TD_THROW_ERROR_IF(false, TD_ERROR_DEFAULT, fail)
	ASSERT_EQ(errorCode, TD_SUCCESS);
	return;

fail:
	ASSERT_FALSE(true);
}

TEST(ThrowIfErrorTest, true) {
	int errorCode = TD_ERROR_DEFAULT;

	TD_THROW_IF_ERROR(success)
	ASSERT_FALSE(true);

success:
	ASSERT_TRUE(true);
}

TEST(ThrowIfErrorTest, false) {
	int errorCode = TD_SUCCESS;

	TD_THROW_IF_ERROR(fail)
	ASSERT_TRUE(true);
	return;

fail:
	ASSERT_FALSE(true);
}

TEST(IgnoreErrorTest, success) {
	int errorCode = TD_ERROR_DEFAULT;

	TD_THROW_IF_ERROR(success)
	ASSERT_FALSE(true);

success:
	TD_IGNORE_ERROR
	ASSERT_EQ(errorCode, TD_SUCCESS);
}
