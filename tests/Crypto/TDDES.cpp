/*****************************************************************************
 * Copyright (c) 2018 Trust Designer. All rights reserved.
 *
 * TDAlgo project
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @author: Antoine Caron
 * @version: 1.0.0
 * @date: 05 12 2018
 ****************************************************************************/


#include <cstring>
#include <gtest/gtest.h>

#include <TDAlgo/Crypto/TDDES.h>
#include <TDAlgo/TDAlgo.h>


static void ASSERT_TABLE_EQ(uint8_t *cmp, uint8_t *ref, uint16_t len) {
	for (uint16_t i = 0; i < len; ++i) {
		ASSERT_EQ(cmp[i], ref[i]);
	}
}

TEST(Crypto_DES3_Tests, one_block) {
	uint8_t key[TD_DES3_KEY_SIZE] = { 0xa5, 0xeb, 0xf6, 0x43, 0x43, 0x0d, 0x45, 0xc8, 0xc2, 0x9f, 0x21, 0xb7,
												 0x0a, 0xeb, 0x23, 0xa4, 0xe9, 0x25, 0x5e, 0x79, 0x1c, 0xbb, 0x65, 0x86 };
	uint8_t clearExpected[TD_DES_BLOCK_SIZE] = { 0x4b, 0x61, 0x50, 0x64, 0x53, 0x67, 0x56, 0x6b };
	uint8_t cipherExpected[TD_DES_BLOCK_SIZE] = { 0x21, 0xf4, 0xdd, 0x59, 0xa0, 0x4a, 0xab, 0x81 };
	uint8_t clear[sizeof(clearExpected)] = { 0 };
	uint8_t cipher[sizeof(cipherExpected)] = { 0 };

	TD_des3_cipher(key, clearExpected, sizeof(clearExpected), cipher);
	ASSERT_TABLE_EQ(cipher, cipherExpected, sizeof(clearExpected));

	TD_des3_uncipher(key, cipher, sizeof(cipher), clear);
	ASSERT_TABLE_EQ(clear, clearExpected, sizeof(clearExpected));
}

TEST(Crypto_DES3_Tests, multiple_blocks) {
	uint8_t key[TD_DES3_KEY_SIZE] = { 0xa5, 0xeb, 0xf6, 0x43, 0x43, 0x0d, 0x45, 0xc8, 0xc2, 0x9f, 0x21, 0xb7,
												 0x0a, 0xeb, 0x23, 0xa4, 0xe9, 0x25, 0x5e, 0x79, 0x1c, 0xbb, 0x65, 0x86 };
	uint8_t clearExpected[] = { 0x29, 0x4e, 0x3d, 0xed, 0x1a, 0x5b, 0x90, 0x25, 0x5d, 0xf6, 0x73, 0x30, 0x27, 0x98, 0x80, 0x06,
										 0x6b, 0x5a, 0x89, 0x52, 0x19, 0xa9, 0x4f, 0x62, 0xab, 0x0f, 0xb5, 0xf0, 0x3a, 0x0b, 0xd4, 0xef,
										 0x95, 0x35, 0x49, 0x9c, 0x8c, 0xeb, 0x14, 0x1c, 0xa8, 0xdb, 0x16, 0x98, 0x04, 0xd8, 0x64, 0x28,
										 0xa7, 0xc2, 0x28, 0x8c, 0x5c, 0x64, 0xf6, 0x09, 0x40, 0xfe, 0xd6, 0x6f, 0x96, 0x52, 0x8d, 0xd9 };
	uint8_t cipherExpected[] = { 0x25, 0x88, 0x4c, 0xa2, 0x17, 0xbd, 0x53, 0x0f, 0xb1, 0x0b, 0x59, 0x4e, 0x81, 0x91, 0x2f, 0x4b,
										  0xa2, 0x84, 0x5e, 0x4a, 0xd9, 0x5a, 0x85, 0xae, 0x95, 0x68, 0x95, 0x92, 0xba, 0xa5, 0xd0, 0x42,
										  0x60, 0xc6, 0x89, 0x07, 0x2e, 0x23, 0x6c, 0xb7, 0x58, 0x39, 0xf3, 0x46, 0x7d, 0x53, 0x0b, 0x0a,
										  0x02, 0xe2, 0x18, 0x88, 0x00, 0x6f, 0x53, 0xa6, 0xea, 0x2a, 0x7f, 0xef, 0xc5, 0xbd, 0x6c, 0x21 };
	uint8_t clear[sizeof(clearExpected)] = { 0 };
	uint8_t cipher[sizeof(cipherExpected)] = { 0 };

	ASSERT_EQ(sizeof(clearExpected) % TD_DES_BLOCK_SIZE, 0);
	ASSERT_EQ(sizeof(cipherExpected) % TD_DES_BLOCK_SIZE, 0);

	TD_des3_cipher(key, clearExpected, sizeof(clearExpected), cipher);
	ASSERT_TABLE_EQ(cipher, cipherExpected, sizeof(clearExpected));

	TD_des3_uncipher(key, cipher, sizeof(cipher), clear);
	ASSERT_TABLE_EQ(clear, clearExpected, sizeof(clearExpected));
}
