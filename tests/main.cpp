/**
 * Copyright (c) 2016 Trust Designer
 *
 * This software is confidential and proprietary to Trust Designer SAS.
 * It has been furnished under a license and may be used, copied, or
 * disclosed only in accordance with the terms of that license and with
 * the inclusion of this header. No title to nor ownership of this
 * software is hereby transferred.
 *
 * @project : 	TDAlgo
 * @file : 		main.cpp
 *
 * @author: Antoine Caron
 * @version :  	0.0.1
 * @brief :
 * Entry point to run all test suites
 **/

#include <cstring>
#include <gtest/gtest.h>

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}